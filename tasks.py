import os

from invoke import Collection, Task, task

BASE_DIR = os.path.dirname(os.path.abspath(__file__))


def _initialize_django():
    from django.apps import apps

    if not apps.ready:
        os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings')
        import django

        django.setup()


def manage(ctx, dev=False, *args):
    if dev:
        os.environ.setdefault('BROUYASTORE_DEBUG', 'True')
    cmd = '{BASE_DIR}/manage.py '.format(BASE_DIR=BASE_DIR)
    ctx.run(cmd + ' '.join(args))


def check_and_install_packages(ctx, reqs_file):
    try:
        import pip_api
    except ImportError:
        ctx.run('pip install pip_api')
        import pip_api

    from packaging.utils import canonicalize_name

    absolute_req_file = os.path.join(BASE_DIR, reqs_file)
    reqs = set(map(canonicalize_name, pip_api.parse_requirements(absolute_req_file)))
    deps = set(map(canonicalize_name, pip_api.installed_distributions()))
    if not all(req in deps for req in reqs):
        ctx.run('pip install -r {}'.format(absolute_req_file))


@task
def test(ctx, verbose=False):
    """Run test suite"""
    check_and_install_packages(ctx, 'reqs/test.txt')
    ctx.run('pytest' + (' -v' if verbose else ''))


@task
def install(ctx, dev=False):
    """Install Python dependencies"""
    reqs_file = 'reqs/{}.txt'.format('dev' if dev else 'base')
    check_and_install_packages(ctx, reqs_file)


@task
def githooks(ctx):
    """Install and configure git commit hooks"""
    ctx.run(os.path.join(BASE_DIR, '.githooks', 'githooks.py') + ' install --force --clean')


@task(githooks)
def check(ctx, verbose=False):
    """Manually run git pre-commit hook"""
    ctx.run(os.path.join(BASE_DIR, '.githooks', 'pre-commit') + (' --verbose' if verbose else ''))


@task(githooks)
def format(ctx, verbose=False):
    """Manually run git pre-commit hook"""
    ctx.run(os.path.join(BASE_DIR, '.githooks', 'pre-commit') + ' format-black' + (' --verbose' if verbose else ''))
    ctx.run(os.path.join(BASE_DIR, '.githooks', 'pre-commit') + ' format-isort' + (' --verbose' if verbose else ''))


@task(githooks)
def lint(ctx, verbose=False):
    """Manually run git pre-commit hook"""
    ctx.run(os.path.join(BASE_DIR, '.githooks', 'pre-commit') + ' lint-flake8' + (' --verbose' if verbose else ''))


@task(githooks)
def setup(ctx):
    '''Setup the environment for project development'''
    init(ctx, dev=True)


@task
def migrate(ctx, dev=False):
    """Run database migrations so the database schema matches the Django ORM"""
    _initialize_django()
    from core.utils import is_database_synchronized

    if is_database_synchronized():
        return

    manage(ctx, dev, 'migrate')


@task
def collect(ctx, dev=False):
    """Collect static files needed to run the Django server"""
    manage(ctx, dev, 'collectstatic --noinput')


@task
def init(ctx, dev=False):
    """Install dependencies, run database migrations, and collect static files in preparation for running server"""
    install(ctx, dev=dev)
    migrate(ctx, dev=dev)
    collect(ctx, dev=dev)


@task
def shell(ctx, dev=False):
    """Start an interactive Django shell"""
    try:
        import django_extensions  # noqa: F401

        manage(ctx, dev, 'shell_plus')
    except ImportError:
        manage(ctx, dev, 'shell')


@task
def clean(ctx):
    """Purge errant .pyc files and __pycache__ folders and their contents"""
    try:
        import django_extensions  # noqa: F401

        manage(ctx, True, 'clean_pyc')
    except ImportError:
        ctx.run('find {} -name *.pyc -delete'.format(BASE_DIR))
    ctx.run(r'find {} -name __pycache__ -prune -exec rm -rf "{{}}" \;'.format(BASE_DIR))


@task
def start(ctx, dev=False, external=False, *args):
    """Run the Django webserver"""
    init(ctx, dev=dev)
    manage(ctx, dev, 'runserver{}{}'.format('_plus' if dev else '', ' 0.0.0.0:8000' if external else ''))


ns = Collection(*filter(lambda f: isinstance(f, Task), locals().values()))
ns.configure({'run': {'pty': True}})
