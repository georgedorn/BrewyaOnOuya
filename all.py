# Automatically imports all the things you might want to interact with at the shell

from django.apps import apps
from django.conf import settings

from core.decorators import endpoint, memoprop, methods, route
from core.routers import API, register
from core.settings_helpers import IPV4Range, IPV4RangeList, set_and_load_secret_key
from core.urls import urlpatterns
from core.utils import json_urls, simple_json_response
from store.views import consoles, key, premium_purchases, sessions, status

# Import all models
for cfg in apps.get_app_configs():
    for model in cfg.get_models():
        locals()[model.__name__] = model
