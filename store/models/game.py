# -*- coding: utf-8 -*-
from django.db import models
from rest_framework import serializers, viewsets

from store.routing import endpoint, register

# This is just a sample for the moment.  More actual content to come here as the store develops


class Game(models.Model):
    title = models.CharField(max_length=256)
    latest_version = models.CharField(max_length=256, null=True)
    md5sum = models.CharField(max_length=256, null=True)
    apkFileSize = models.IntegerField(null=True)
    uuid = models.UUIDField(null=True)


class GameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Game
        fields = '__all__'


@register('games')
class GameViewset(viewsets.ModelViewSet):
    serializer_class = GameSerializer
    queryset = Game.objects.all()
