from django.conf.urls import include, url

from core import utils
from store import routing, views

urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^', include(routing.urlpatterns)),
    url(r'^api/v1/', include(routing.router.urls)),
    url(r'^api/v1', include(utils.json_urls(views.RESPONSE_MAP))),
]
