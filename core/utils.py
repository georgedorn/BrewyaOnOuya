import os
from functools import wraps

from django.conf.urls import url
from django.db import DEFAULT_DB_ALIAS, connections
from django.db.migrations.executor import MigrationExecutor
from django.http import FileResponse


def is_database_synchronized(database=DEFAULT_DB_ALIAS):
    """Determine if database has unapplied migrations"""
    connection = connections[database]
    connection.prepare_database()
    executor = MigrationExecutor(connection)
    targets = executor.loader.graph.leaf_nodes()
    return False if executor.migration_plan(targets) else True


def json_urls(responses):
    """Dynamically generates urlpatterns mapping to a collection of JSON responses"""
    return [url(route + '$', simple_json_response(endpoint), name=endpoint) for route, endpoint in responses.items()]


def memoprop(func):
    """
    Creates a memoized property out of a method

    Memoization caches the result of calling a function, and uses the cache to supply the same result later
    A property is a method that acts like an attribute, where you don't have to "call" it with parenthesis.

    Since a property takes no arguments, a memoization cache only needs to hold one value.

    The value of this is that you don't have to initialize values you aren't going to need, nor keep track of
    what has been initialized.  Just supply the formula for calculating some expensive value in a function,
    access it when you need it, and this decorator will keep everything efficient.
    """

    @wraps(func)
    def _inner(self):
        var_name = '_{}'.format(func.__name__)
        if not hasattr(self, var_name):
            setattr(self, var_name, func(self))

        return getattr(self, var_name)

    return property(_inner)


def simple_json_response(endpoint):
    """Dynamically generates view functions that load and return a response from a JSON file on disk"""
    from django.conf import settings

    def _inner(request):
        filename = os.path.join(settings.BASE_DIR, 'store', 'responses', '{}.json'.format(endpoint))
        return FileResponse(open(filename, 'rb'), content_type='application/json')

    return _inner
