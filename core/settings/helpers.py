import os

from django.core.management.utils import get_random_secret_key


class IPV4Range:
    """
    Represents a range of IPV4 addressess

    Accepts an IPV4 address in one of the following forms:
      * Static -- 192.168.1.1
      * Wildcard -- 192.168.*.10
      * CIDR block extension -- 192.168.1.1/24
      * CIDR block level -- /2  (Has two leading one bits, so would look like 192.0.0.0/2)

    Use the `in` keyword to test if a single discreet IPV4 address falls within the initialized range
    """

    def __init__(self, address):
        self.address = address

        addr, _, block_level = address.strip().partition('/')

        if '*' in addr and block_level:
            raise Exception('Cannot use wildcards with CIDR block range notation')

        if addr == '*':
            addr = '*.*.*.*'

        self.octets = list(map(lambda c: c if c == '*' else int(c), addr.split('.'))) if addr else None
        self.block_level = int(block_level) if block_level else None
        if self.block_level:
            self.bitmask = self._bitmask
            self.bitstring = self._bitstring

    def __repr__(self):
        return '{}({!r})'.format(self.__class__.__name__, self.address)

    def __contains__(self, address):
        """
        This is the magic method that makes the `in` keyword work.

        Use it like this:

        ```
        >>> ipv4_range = IPV4Range('192.168.0.0/16')
        >>> '192.168.1.132' in ipv4_range
        True
        >>> '173.56.72.138' in ipv4_range
        False
        ```
        """
        octets = map(int, address.strip().split('.'))
        if self.block_level is not None:
            return not ((self.to_number(octets) ^ self.bitstring) & self.bitmask)

        return all(o2 == '*' or o1 == o2 for o1, o2 in zip(octets, self.octets))

    @property
    def _bitmask(self):
        """
        This will give you just the portion of an IP address specified by a CIDR range

        `192.168.1.1` is represented in binary as `11000000 10101000 00000001 00000001`
        If we want to see if an address is in `192.168.1.1/16`, then we don't care about the last 16 bits of the address
        So we generate a bitmask that will extract the first 16 bits:  `11111111 11111111 00000000 00000000`
        Doing a bitwise AND (`&`) between a number and the bitmask will remove all bits from the rightmost two bytes
        """
        return ((1 << self.block_level) - 1) << (32 - self.block_level)

    @property
    def _bitstring(self):
        """Stores the bitstring for the range this object represents"""
        return self.to_number(self.octets) if self.octets else self.bitmask

    @staticmethod
    def to_number(octets):
        """Convert IP address in octet-notation to a single 32-bit number"""
        return sum(o << (8 * (3 - i)) for i, o in enumerate(octets))


class IPV4RangeList(list):
    """Creates a list of IPV4Range objects, allowing you to create a disjoint set of addresses to filter against"""

    def __init__(self, iterable):
        list.__init__(self, map(IPV4Range, iterable))

    def append(self, ipv4_range):
        list.append(self, IPV4Range(ipv4_range))

    def __contains__(self, address):
        return any(address in ipv4_range for ipv4_range in self)


def set_and_load_secret_key(directory, keyfile):
    """Returns a secret key that is cached in a file on disk"""
    try:
        with open(os.path.join(directory, keyfile), 'rb') as f:
            return f.read().decode('utf-8').strip()
    except IOError:
        secret_key = get_random_secret_key()
        with open(os.path.join(directory, keyfile), 'wb') as f:
            f.write(secret_key.encode('utf-8'))
        return secret_key
