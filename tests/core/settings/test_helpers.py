from core.settings.helpers import IPV4Range, IPV4RangeList

# TODO: Write lots more tests:
# * Unit
# * Functional
# * Behavioral (Boundary)
# * VCR
# * Black-box
# * Fuzz
# * Branch
# * Component
# * End-to-end
# * Happypath
# * Mutation


class TestIPV4Range:
    def test_static_ip(self):
        ip_range = IPV4Range('192.168.1.243')
        assert '192.168.1.243' in ip_range
        assert '173.246.8.104' not in ip_range

    def test_cidr_subnet(self):
        ip_range = IPV4Range('192.168.0.0/16')
        assert '192.168.100.10' in ip_range
        assert '192.168.0.1' in ip_range
        assert '10.0.0.1' not in ip_range
        assert '192.167.100.10' not in ip_range

        ip_range = IPV4Range('172.16.0.0/12')
        assert '172.17.0.1' in ip_range

    def test_cidr_only_notation(self):
        ip_range = IPV4Range('/2')
        assert ip_range.bitmask is not None
        assert '192.168.100.10' in ip_range
        assert '192.168.0.1' in ip_range
        assert '10.0.0.1' not in ip_range
        assert '192.167.100.10' in ip_range

    def test_wildcards(self):
        ip_range = IPV4Range('192.168.*.*')
        assert '192.168.100.10' in ip_range
        assert '192.168.0.1' in ip_range
        assert '10.0.0.1' not in ip_range
        assert '192.167.100.10' not in ip_range

    def test_single_wildcard(self):
        ip_range = IPV4Range('*')
        assert '192.168.100.10' in ip_range
        assert '192.168.0.1' in ip_range
        assert '10.0.0.1' in ip_range
        assert '192.167.100.10' in ip_range


class TestIPV4RangeList:
    def test_cidr_subnet(self):
        ip_range = IPV4RangeList(['172.16.0.0/12'])
        assert '172.17.0.1' in ip_range
